# php+nginx+redis
FROM php:7.4-fpm
COPY sources.list /etc/apt/sources.list
COPY start.sh /root/start.sh
RUN apt-get clean up \
    && apt-get update -y \
    && apt-get install -y libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    redis-server \
    nginx \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd
RUN pecl install redis \
    && docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-enable redis