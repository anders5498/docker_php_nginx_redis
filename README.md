#  一键生成PHP本地开发环境 nginx+php+redis

1. 修改`docker-compose.yml`第7行，改成自己的项目目录

2. 修改`docker-compose.yml`对外暴露的端口号，根据nginx配置来调整

3. 在`nginx/conf.d`目录下创建nginx配置文件

4. 当前目录执行命令`docker-compose up -d`

5. 进入容器中，执行命令`sh /root/start.sh`
